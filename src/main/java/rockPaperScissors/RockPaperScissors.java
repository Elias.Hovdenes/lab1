package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> yOrNo = Arrays.asList("y", "n");

    public void run() {
        while (true){
            System.out.printf("Let's play round %x \n", roundCounter);
            //System.out.println("\n");

            String humanChoice = userChoice();
            String computerChoice = randomChoice();
            //System.out.println(humanChoice+computerChoice);

            String choiceString = "Human chose "+ humanChoice +", computer chose " + computerChoice + ".";

            if (isWinner(humanChoice,computerChoice)){
                System.out.println(choiceString + " Human wins!");
                humanScore+=1;
            }
            else if (isWinner(computerChoice,humanChoice)){
                System.out.println(choiceString + " Computer wins!");
                computerScore+=1;
            }
            else{
                System.out.println(choiceString + " It's a tie!");
            }
            System.out.println("Score: human "+ computerScore + ", computer " + humanScore);
            
            String continueAnswer = continuePlaying();
            if (continueAnswer.equals("n")){
                break;
            }

            roundCounter+=1;

        } 
        System.out.println("Bye bye :)");
        
    }

    public String continuePlaying(){
        while (true){
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
    
            if (validateInput(continueAnswer,yOrNo)){
                return continueAnswer;
            }
            else{
                System.out.println("I don't understand " + continueAnswer + ". Could you try again?");
            }
        }
    }

    public boolean isWinner(String humanChoice, String computerChoice){
        if (humanChoice.equals("paper")){
            return computerChoice.equals("rock");
        }
        else if (humanChoice.equals("scissors")){
            return computerChoice.equals("paper");
        }
        else{
            return computerChoice.equals("scissors");
        }
    }

    public String randomChoice(){
        
        Random rand = new Random();
        int rpsIndexes = rpsChoices.size();

        String comChoice = rpsChoices.get(rand.nextInt(rpsIndexes));
        
        return comChoice;
    }

    public String userChoice(){
        while (true){
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            if (validateInput(humanChoice, rpsChoices)){
                return humanChoice;
            }
            else{
                System.out.printf("I don't understand %s. Could you try again \n", humanChoice);
            }


        }

    }
                                                                                                      
    public boolean validateInput(String input, List<String> validInput){
        return validInput.contains(input.toLowerCase());
    }





    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}